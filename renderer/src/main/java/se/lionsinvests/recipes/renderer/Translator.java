package se.lionsinvests.recipes.renderer;

public interface Translator<A> {

    String translate(A obj);
}

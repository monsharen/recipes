metadata:
  name: Lussebullar
  images:
    - https://i.imgur.io/iEaZ9s6_d.webp?maxwidth=640&shape=thumb&fidelity=medium
  description: From https://www.juligen.se/ultimata-lussebullarna-med-fordeg/ 
  types: [Bread]
  estimatedPrepTime: 10 h
  servings: 30


ingredients:
 - 5 dl mjölk (till fördeg)
 - 1 g saffran (mortlat med lite socker och 1,5 msk rom eller annan sprit)
 - 40 g jäst (blå)
 - 600 g vetemjöl special (eller annat vetemjöl av bra kvalitet) (till fördeg)
 - 1 tbsp socker eller vaniljsocker
 - 4 tbsp russin 
 - 1 dl glögg
 - 400 g vetemjöl special (eller annat vetemjöl av bra kvalitet)
 - 200 g smör (rumstempererat)
 - 1 u ägg (rumstempererat)
 - 200 g socker
 - 5 g salt (0,75-1 tsk)
 - 1 u ägg + 2 msk vatten och en nypa salt till pensling

actions:
 - divider Dag 1
 - free_text Mortla 1 g saffran med lite socker och låt den dra i rom eller konjak för att lösa upp den ännu mer.
 - free_text Rör ut 40 g jäst med saffransblandningen, 1 msk vaniljsocker och 2 dl mjölk.
 - image https://www.juligen.se/wp-content/uploads/2019/10/Lussebullar_fordeg1.jpg
 - image https://www.juligen.se/wp-content/uploads/2019/10/Lussebullar_fordeg2.jpg
 - image https://www.juligen.se/wp-content/uploads/2019/10/Lussebullar_fordeg3.jpg
 - free_text Tillsätt 3 dl mjölk och 600 g mjöl. Rör i några minuter.
 - free_text Låt helst jäsa över natten i kylen. Tänk på att degen expanderar så ha gärna en ganska stor bunke.
 - free_text Ta ut 200 g smör och 2 ägg ur kylen så att de är rumstempererade till nästa dag.
 - divider Dag 2
 - image https://www.juligen.se/wp-content/uploads/2019/10/Lussebullsdeg_jasning.jpg, Dag 2 ser degen ut ungefär så här
 - free_text Koka upp 1 dl glögg, dra åt sidan och lägg i 4 msk russin.
 - free_text Arbeta in 200 g smör, 400 g mjöl, 1 ägg och 200 g socker i degen och 400 g mjöl (lite i taget). Degen ska kännas kladdig men släppa någorlunda från kanterna. Det är inte säkert att allt mjöl behövs.
 - free_text Knåda degen i minst 20-30 minuter för hand eller 15-20 minuter i maskin. Kör långsammare i början.
 - free_text Tillsätt 5 g salt på slutet av knådningen.
 - image https://www.juligen.se/wp-content/uploads/2019/10/Glutentest.jpg, Gör glutentestet för att ta reda på om degen är färdig. Se video för instruktioner.
 - youtube https://www.youtube.com/embed/0CFXLJpTPb0?si=bh60sx3nyH-UaYau
 - free_text Låt degen vila i 30 minuter under plast eller handduk.
 - free_text Sätt ugnen på 220 C med över- och undervärme.
 - free_text Häll upp på bakbord, använd gärna degskrapa. Dela i bitar om ca 60 gram. Forma till lussekatter.
 - free_text Garnera med russin genom att trycka ner dem en bit så att de inte lossnar när bullarna blir stora i ugnen.
 - free_text Låt jäsa övertäckta i minst 1 timme till dubbel storlek. Jästiden beror bland annat på hur varmt du har i rummet. Undvik luftdrag vid fönster och liknande.
 - image https://www.juligen.se/wp-content/uploads/2019/10/Lussekatter_pa_plat.jpg
 - free_text Grädda i ca 6-8 minuter i nedre delen av ugnen. Temperaturen kan skilja från ugn till ugn så håll gärna koll på bullarna.
 - free_text Pensla direkt med ägget uppvispat med vatten och en liten nypa salt. Ta bort bullarna från plåten så att de inte eftergräddas. Frys in så snart bullarna har kallnat.
 - image https://www.juligen.se/wp-content/uploads/2019/10/Lussebullar_galler.jpg

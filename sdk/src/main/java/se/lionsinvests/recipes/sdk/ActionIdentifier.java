package se.lionsinvests.recipes.sdk;

public enum ActionIdentifier {
    FREE_TEXT,
    IMAGE,
    YOUTUBE,
    DIVIDER
}

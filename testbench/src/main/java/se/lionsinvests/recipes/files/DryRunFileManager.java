package se.lionsinvests.recipes.files;

import java.io.File;

public class DryRunFileManager implements FileManager {

    @Override
    public void writeToFile(String targetFileName, String html) {

    }

    @Override
    public void writeRecipeToFile(String targetFileName, String html) {

    }

    @Override
    public void exportResource(String resourceName) {

    }

    @Override
    public void exportResource(String resourceName, File outputFile) throws Exception {

    }
}

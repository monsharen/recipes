# Welcome
This is a personal recipe site without the bloat, that promotes careful curation and content improvement over time. 

Live demo: http://monsharen.gitlab.io/recipes

# What
An interpreter and template engine compiles recipe files into static HTML pages. Ideal content for hosting on gitlab or github pages.

Using pipeline definitions the recipe book is recompiled and deployed on commit. Invalid recipe definitions lead to build failure to support your favorite branching strategy.

# How to get started
Clone the repository to start your own recipe book.

# Build steps
1. Add recipe files in the recipes folder
2. Compile to regenerate the static recipe html pages



